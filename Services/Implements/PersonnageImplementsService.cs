﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Services.Implements
{
    using Models;
    using Repositories;
    public class PersonnageImplementsService : PersonnageService
    {
        private PersonnageRepository repository;

        public PersonnageImplementsService(PersonnageRepository repository)
        {
            this.repository = repository;
        }
        public Personnage Modifier(Personnage p)
        {
            return this.repository.Update(p);
        }
        public Personnage Sauvegarde(Personnage p)
        {
            return this.repository.Save(p);
        }
        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }
        public IEnumerable<Personnage> TrouverTous()
        {
            return this.repository.FindAll();
        }
        public Personnage TrouverUn(int id)
        {
            return this.repository.FindById(id);
        }
    }
}
