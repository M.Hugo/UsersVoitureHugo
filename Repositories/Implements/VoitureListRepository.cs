﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Repositories.Implements
{
    using Models;
    public class VoitureListRepository
    {
        private List<Voiture> voitures = new List<Voiture>();

        public void Delete(int id)
        {
            this.voitures[id] = null;
        }
        public void Delete(Voiture p)
        {
            this.voitures[p.Id] = null;
        }
        public IEnumerable<Voiture> FindAll()
        {
            return this.voitures.Where(p => p != null);
        }
        public Voiture FindById(int id)
        {
            return this.voitures[id];
        }
        public Voiture Save(Voiture p)
        {
            p.Id = this.voitures.Count();
            this.voitures.Add(p);
            return p;
        }
        public Voiture Update(Voiture p)
        {
            this.voitures[p.Id] = p;
            return p;
        }
    }
}
