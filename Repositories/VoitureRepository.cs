﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Repositories
{
    using Models;
    public interface VoitureRepository
    {
        public Voiture Save(Voiture p);
        public IEnumerable<Voiture> FindAll();
        public Voiture FindById(int id);
        public Voiture Update(Voiture p);
        public void Delete(int id);
        public void Delete(Voiture p);
    }
}
